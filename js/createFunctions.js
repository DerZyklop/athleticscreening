
// ----- WELCOME-PAGE ----- WELCOME-PAGE ----- WELCOME-PAGE ----- WELCOME-PAGE ----- WELCOME-PAGE ----

// var html = new sap.ui.core.HTML({
//     content: "<h2 style='color:green; text-align:center' >Willkommen</br> zur <b>Athletik Screening</b> App</h2>"
// });



// var welcomeItemList = new sap.m.List({
//     items: [
//         new sap.m.StandardListItem({
//             type: sap.m.ListType.Navigation,
//             title: "Was ist Athletik Screening",
//             press: function(evt){
//                 app.to(pageAbout);
//             }
//         }),
//         new sap.m.StandardListItem({
//             type: sap.m.ListType.Navigation,
//             title: "Athletik Camp Website"
//         }),
//         new sap.m.StandardListItem({
//             type: sap.m.ListType.Navigation,
//             title: "Hauptmenü",
//             press: function(evt){
//                 createMenu(evt);
//             }
//         })
//     ]
// });


// var brkHtml = new sap.ui.core.HTML({
//     content: "</br>"
// });

// var btn1Html = new sap.ui.core.HTML({
//     content: "<button style='width:100%; height:10%; background-color:#D8F6CE'>Was ist Athletik Screening?</Button>"
// });

// var btn2Html = new sap.ui.core.HTML({
//     content: "<button>Athletik Camp Website</Button>"
// });
// var btn3Html = new sap.ui.core.HTML({
//     content: "<button>Hauptmenü<B/utton>"
// });


// var welcomePanel = new sap.m.Panel({
//     //areaDesign: sap.ui.commons.enums.AreaDesign.Fill,
//     content: [welcomeLabel],
//     height: "60%"
// });



// ----- ABOUT-PAGE ----- ABOUT-PAGE ----- ABOUT-PAGE ----- ABOUT-PAGE ----- ABOUT-PAGE ----- ABOUT-PAGE



var aboutPanel = new sap.m.Panel({
    //headerText: aboutTitle,
    content: []
});

// ---------------- PAGE-MENU --------------------  ---------------- PAGE-MENU --------------------  ---------------- PAGE-MENU --------------------

var screeningList = new sap.m.List({ width: "100%"});
var trainingList = new sap.m.List();


var modus = null;
var tabMenu = new sap.m.IconTabBar("tabMenu", {
    items: [
        new sap.m.IconTabFilter("screeningTab", {
            text: "Screening",
            content: screeningList

        }),
        new sap.m.IconTabFilter("trainingTab", {
            text: "Trainingsplan",
            content: trainingList
        })
    ],
    select: function(evt){
       //alert(evt.getSource().getBindingContext().getObject('Text'));
    }
});

var menuToolbar = new sap.m.Toolbar({
    content: [
        new sap.ui.core.Icon({
                src: sap.ui.core.IconPool.getIconURI("settings")
            }),
            new sap.m.ToolbarSpacer(),
            new sap.m.Button({
                text: "Beginnen",
                press: function(evt){
                    switch((app.getCurrentPage().getContent()[0]).getSelectedKey()){
                        case "screeningTab":
                        console.log("crScreening");
                           createScreening();
                            break;
                        case "trainingTab":
                            createTrPlan();
                            break;
                     }

                }
            })
    ]
});



// ---------------- PAGE-TEST -------------------- ---------------- PAGE-TEST -------------------- ---------------- PAGE-TEST --------------------
var testDescList = new sap.m.List();

var questionList = new sap.m.List();

var testToolbar = new sap.m.Toolbar({
    content: [
        new sap.m.Button({
            text: "Zurück",
            icon: sap.ui.core.IconPool.getIconURI("arrow-left"),
            iconFirst: true
        }),
        new sap.m.ToolbarSpacer(),
        new sap.m.Button({
            text: "Weiter",
            icon: sap.ui.core.IconPool.getIconURI("arrow-right"),
            iconFirst: false
        })
    ]
});
// -------------------------------------------------------------------------------------------------------------------------------------------------

// ------------ PAGE-QUENSTIONINFO ------------- ------------ PAGE-QUENSTIONINFO ------------- ------------ PAGE-QUENSTIONINFO -------------


var qInfoToolbar = new sap.m.Toolbar({
    content: [
        new sap.m.Button({
            text: "JA",
            width: "50%",
            type: "Accept"
            // icon: sap.ui.core.IconPool.getIconURI("arrow-left"),
            // iconFirst: true
        }),
        new sap.m.ToolbarSpacer(),
        new sap.m.Button({
            text: "NEIN",
            width: "50%",
            type: "Reject"
            // icon: sap.ui.core.IconPool.getIconURI("arrow-right"),
            // iconFirst: false
        })
    ]
});
// -----------------------------------------------------------------------------------------------------------------------------------------



// ---------------- PAGE-TESTINFO ---------------- ---------------- PAGE-TESTINFO ---------------- ---------------- PAGE-TESTINFO ----------------


var imgCarousel = new sap.m.Carousel();

var testDetailDesc = new sap.m.Text({

    text: "Lorem ipsum dolor st amet, consetetur sadipscing elitr,"
                    + "sed diam nonumy eirmod tempor invidunt ut labore et dolore"
                    + "magna aliquyam erat, sed diam voluptua. At vero eos et accusam et"
                    + "justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea"
                    + "takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet,"
                    + "consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore"
                    + "et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet,"
                    + "consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore "
                    + "et dolore magna aliquyam erat"
});

var testInfoPanel = new sap.m.Panel({
    headerText: "Butterfly",
    height: "100%",
    content: [
        imgCarousel, testDetailDesc
    ]
});



// ----------------- PAGE-AUSWERTUNG ------------- ----------------- PAGE-AUSWERTUNG ------------- ----------------- PAGE-AUSWERTUNG -------------
var auswertungList = new sap.m.List({
    width: "100%",
    items: [
        new sap.m.CustomListItem({
            width: "100%",
            type: "Navigation",
            content: [
                new sap.m.ProgressIndicator({

                    displayValue: "3/5 Butterfly",
                    percentValue: 60.0,
                    state: "Success"
                })
            ],
            press: function(evt){
                createAuswDetail(evt);
            }
        }),
        new sap.m.CustomListItem({
            type: "Navigation",
            content: [
                new sap.m.ProgressIndicator({

                    displayValue: "5/5 Push Ups",
                    percentValue: 100.0,
                    state: "Success"
                })
            ],
            press: function(evt){
                createAuswDetail(evt);
            }
        }),
        new sap.m.CustomListItem({
            type: "Navigation",
            content: [
                new sap.m.ProgressIndicator({

                    displayValue: "2/5 Hantelstange",
                    percentValue: 40.0,
                    state: "Success"
                })
            ],
            press: function(evt){
                createAuswDetail(evt);
            }
        })
    ]
});

var testToolbar = new sap.m.Toolbar({
    content: [
        new sap.m.Button({
            text: "Zurück",
            icon: sap.ui.core.IconPool.getIconURI("arrow-left"),
            iconFirst: true
        }),
        new sap.m.ToolbarSpacer(),
        new sap.m.Button({
            text: "Weiter",
            icon: sap.ui.core.IconPool.getIconURI("arrow-right"),
            iconFirst: false
        })
    ]
});


var progressbarBar = new sap.m.Toolbar({
    content: [
        new sap.m.ProgressIndicator({
            displayValue: "8/15 Gesamt",
            percentValue: 53.0,
            state: "Success"
        })
    ]
});

var progressList = new sap.m.List({
    items: [
        new sap.m.CustomListItem({
            type: "Active",
            content: [
                progressbarBar
            ],
            press: function(evt){
                toPageHistorie(evt);
            }

        })
    ]
});

var auswertungBar = new sap.m.Bar({
    width: "100%",
    contentMiddle: [
        new sap.m.Button({
            text: "Trainingsplan",
            widht: "95%",
            press: function(evt){
                toUebung(evt);
            }
        })
    ]
});
// -----------------------------------------------------------------------------------------------------------------------------------------------



// ---------------- PAGE-AUSWERTUNGDETAIL ------------------- ---------------- PAGE-AUSWERTUNGDETAIL ------------------- ---------------- PAGE-AUSWERTUNGDETAIL -------------------
var testDescList2 = new sap.m.List();

var qInfoList = new sap.m.List({
    items: [
        new sap.m.CustomListItem({
            type: "Active",
            content: [
                new sap.m.FlexBox({
                    height: "80px",
                    alignItems: "Center",
                    items: [
                        new sap.ui.core.Icon({
                        color: "green",
                        src: "sap-icon://accept"
                        }),
                        new sap.m.Text({
                            text: "Beine druchgestreckt"
                        }),
                        new sap.ui.core.Icon({
                            src: sap.ui.core.IconPool.getIconURI("message-information"),
                            press: function(evt){
                                createQinfo(evt, false);
                            }
                        })
                    ]
                })

            ]
        }),
        new sap.m.CustomListItem({
            type: "Active",
            content: [
                new sap.ui.core.Icon({
                    color: "red",
                    src: sap.ui.core.IconPool.getIconURI("decline")
                }),
                new sap.m.Text({
                    text: "Rücken gerade"
                }),
                new sap.ui.core.Icon({
                    src: sap.ui.core.IconPool.getIconURI("message-information"),
                    press: function(evt){
                        createQinfo(evt, false);
                    }
                })
            ]
        }),
        new sap.m.CustomListItem({
            type: "Active",
            content: [
                new sap.ui.core.Icon({
                    color: "green",
                    src: sap.ui.core.IconPool.getIconURI("accept")
                }),
                new sap.m.Text({
                    text: "Beine druchgestreckt"
                }),
                new sap.ui.core.Icon({
                    src: sap.ui.core.IconPool.getIconURI("message-information"),
                    press: function(evt){
                        createQinfo(evt, false);
                    }
                })
            ]
        })
    ]
});

var auswDetailToolbar = new sap.m.Toolbar({
    content: [
        new sap.m.Button({
            text: "Zurück",
            icon: sap.ui.core.IconPool.getIconURI("arrow-left"),
            iconFirst: true
        }),
        new sap.m.ToolbarSpacer(),
        new sap.m.Button({
            text: "Weiter",
            icon: sap.ui.core.IconPool.getIconURI("arrow-right"),
            iconFirst: false
        })
    ]
});
// -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



// ------------- PAGE-TRAININGSPLAN -------------- ------------- PAGE-TRAININGSPLAN -------------- ------------- PAGE-TRAININGSPLAN --------------
var uText1 = new sap.m.Text({ text: "Bauch" });
var uText2 = new sap.m.Text({ text: "Beine" });

var uList1 = new sap.m.List({
    items: [
        new sap.m.StandardListItem({
            type: "Active",
            title: "Drücken",
            description: "3 * 15 Wiederholungen",
            press: function(evt){
                toUebungInfo(evt);
            }
        }),
        new sap.m.StandardListItem({
            type: "Active",
            title: "Ziehen",
            description: "5 * 20 Wiederholungen",
            press: function(evt){
                toUebungInfo(evt);
            }
        }),
        new sap.m.StandardListItem({
            type: "Active",
            title: "Machen",
            description: "5 * 10 Wiederholungen",
            press: function(evt){
                toUebungInfo(evt);
            }
        })
    ]
});

var uList2 = new sap.m.List({
    items: [
        new sap.m.StandardListItem({
            type: "Active",
            title: "Drücken",
            description: "3 * 15 Wiederholungen",
            press: function(evt){
                toUebungInfo(evt);
            }
        }),
        new sap.m.StandardListItem({
            type: "Active",
            title: "Ziehen",
            description: "5 * 20 Wiederholungen",
            press: function(evt){
                toUebungInfo(evt);
            }
        }),
        new sap.m.StandardListItem({
            type: "Active",
            title: "Machen",
            description: "5 * 10 Wiederholungen",
            press: function(evt){
                toUebungInfo(evt);
            }
        })
    ]
});

var planBar = new sap.m.Bar({
    contentRight: new sap.m.Button({
        text: "Beginnen",
        press: function(evt){
            toUebung(evt);
        }
    })
});
// -----------------------------------------------------------------------------------------------------------------------------------------------



// ------- PAGE-ÜBUNGDETAILS -------- ------- PAGE-ÜBUNGDETAILS -------- ------- PAGE-ÜBUNGDETAILS -------- ------- PAGE-ÜBUNGDETAILS --------
var uICarousel = new sap.m.Carousel({
    height: "30%",
    pages: [
        new sap.m.Image({
            src: "img/zwei.jpg",
            width: "100%"
        }),
        new sap.m.Image({
            src: "img/drei.jpg",
            width: "100%"
        }),
        new sap.m.Image({
            src: "img/vier.jpg",
            width: "100%"
        })
    ]
});

var uDetailDesc = new sap.m.Text({

    text: "Lorem ipsum dolor st amet, consetetur sadipscing elitr,"
                    + "sed diam nonumy eirmod tempor invidunt ut labore et dolore"
                    + "magna aliquyam erat, sed diam voluptua. At vero eos et accusam et"
                    + "justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea"
                    + "takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet,"
                    + "consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore"
                    + "et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet,"
                    + "consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore "
                    + "et dolore magna aliquyam erat"
});

var uInfoPanel = new sap.m.Panel({
    headerText: "Butterfly",
    height: "100%",
    content: [
        uICarousel, uDetailDesc
    ]
});
// -------------------------------------------------------------------------------------------------------------------------------------------



// ---------------- PAGE-HISTORIE ---------------- ---------------- PAGE-HISTORIE ---------------- ---------------- PAGE-HISTORIE -------------

// var oDataset = new sap.viz.ui5.data.FlattenedDataset({
//             dimensions: [{
//                 name: "Datum",
//                 value: "{datum}"
//             }],
//             measures: [{
//                 name: 'Leistung',
//                 value: '{leistung}'
//             }],
//             data: {
//                 path: "/historieDaten"
//             }
//         });


// var lineChart = new sap.viz.ui5.Line({
//     widht: "100%",
//     height: "50%"
// });
// -----------------------------------------------------------------------------------------------------------------------------------------



// ---------------- PAGE-UEBUNG ------------------ ---------------- PAGE-UEBUNG ------------------ ---------------- PAGE-UEBUNG ---------------
var uTitelFlexBox = new sap.m.FlexBox({
    alignItems: "Start",
    justifyContent: "SpaceBetween",
    width: "90%",
    items: [
        new sap.m.Text({
            text: "2/5 Ziehen"
        }),
        new sap.ui.core.Icon({
            color: "green",
            src: "sap-icon://message-information"
        })
    ]
});

var uCarousel = new sap.m.Carousel({
    height: "50%",
    pages: [
        new sap.m.Image({
            src: "img/zwei.jpg",
            height: "5%",
            press: function(evt){
                (this.getParent()).setHeight("90%");
                this.setHeight("10%");
            }
        }),
        new sap.m.Image({
            src: "img/drei.jpg",
            height: "5%"
        }),
        new sap.m.Image({
            src: "img/vier.jpg",
            height: "5%"
        })
    ]
});

var uBar = new sap.m.Toolbar({
    content: [
        new sap.m.Button({
            text: "Zurück",
            icon: sap.ui.core.IconPool.getIconURI("arrow-left"),
            iconFirst: true
        }),
        new sap.m.ToolbarSpacer({
            text: "Weiter",
            icon: sap.ui.core.IconPool.getIconURI("arrow-right"),
            iconFirst: false
        })
    ]
});
// --------------------------------------------------------------------------------------------------------------------------------------------



// ********************************************************************************************************************************************
// ********************************************************************************************************************************************
// ---------------- FUNKTIONEN ------------------- FUNKTIONEN ------------------- FUNKTIONEN ------------------- FUNKTIONEN -------------------
// ********************************************************************************************************************************************
// ********************************************************************************************************************************************

function createMenu(evt){
    //getMenuData();
    // app.to(pageMenu);

    // var screeningModel = new sap.ui.model.json.JSONModel();
    // //screeningModel.setData(data.Screening);
    // screeningList.setModel(screeningModel);
    // trainingList.setModel(screeningModel);

    // var historieModel = new sap.ui.model.json.JSONModel();
    // historieModel.setData(data.Historie);
    // sap.ui.getCore().setModel(historieModel, "historie");

    //bindMenuItems();

}

function bindMenuItems(dataResults){
    var menuModel = new sap.ui.model.json.JSONModel();
    menuModel.setData(dataResults);
    screeningList.setModel(menuModel, "menuModel");
    //trainingList.setModel(menuModel, "menuModel");
    screeningList.bindItems({
            path: "menuModel>/",
            sorter: new sap.ui.model.Sorter("lfdnr"),
            filters: [ new sap.ui.model.Filter("Genre", sap.ui.model.FilterOperator.EQ, "Menu")],
            template: new sap.m.CustomListItem({
                content: [
                        new sap.m.FlexBox({
                            direction: "Column",
                            alignItems: "Start",
                            items: [
                                new sap.ui.core.HTML({content: "</br>"}),
                                new sap.m.Text({ text: "{menuModel>Text}"}),
                                new sap.ui.core.HTML({content: "<hr>"}),
                                new sap.m.FlexBox({
                                    alignItems: "Start",
                                    justifyContent: "SpaceBetween",
                                    width: "100%",
                                    items: [
                                        new sap.m.Switch({
                                            state: false
                                        }),
                                        new sap.m.Button({
                                            width: "100%",
                                            text: "3/9",
                                            press: function(evt){
                                                createAuswertung(evt);
                                            }
                                        })

                                    ]
                                }),
                                new sap.ui.core.HTML({content: "</br>"})
                            ]
                        })
                ]
            })
         });

    trainingList.bindItems({
            path: "menuModel>/",
            sorter: new sap.ui.model.Sorter("lfdnr"),
            filters: [ new sap.ui.model.Filter("Genre", sap.ui.model.FilterOperator.EQ, "Menu")],
            template: new sap.m.CustomListItem({
                content: [
                        new sap.m.FlexBox({
                            direction: "Column",
                            alignItems: "Start",
                            items: [
                                new sap.m.Text({ text: "{menuModel>Text}"}),
                                new sap.m.FlexBox({
                                    alignItems: "Start",
                                    justifyContent: "SpaceBetween",
                                    width: "100%",
                                    items: [
                                        new sap.m.Switch({
                                            state: false
                                        }),
                                        new sap.m.Button({
                                            width: "100%",
                                            text: "1/2"
                                        })

                                    ]
                                })
                            ]
                        })
                ]
            })
         });


}

function createScreening(id){
    app.to(pageTest);
    questionList.removeAllItems();
    var questionItem1 = new sap.m.CustomListItem({
        content: [
            new sap.m.FlexBox({
                direction: "Column",
                alignItems: "Start",
                items: [
                    new sap.m.Text({ text: "Beine durchgesreckt"}),
                    new sap.m.FlexBox({
                        alignItems: "Start",
                        justifyContent: "SpaceBetween",
                        width: "100%",
                        items: [
                            new sap.m.Switch({
                                state: false
                            }),
                            new sap.m.Button({
                                customData: [
                                    new sap.ui.core.CustomData({
                                        key: "hidden",
                                        value: "yes"
                                    })
                                ],
                                text: "Info",
                                press: function(evt){
                                    createQinfo(evt, true);

                                }
                            })
                        ]
                    })
                ]
            })
        ]

    });
    var questionItem2 = new sap.m.CustomListItem({
        content: [
            new sap.m.FlexBox({
                direction: "Column",
                alignItems: "Start",
                items: [
                    new sap.m.Text({ text: "Ferse auf dem Boden"}),
                    new sap.m.FlexBox({
                        alignItems: "Start",
                        justifyContent: "SpaceBetween",
                        width: "100%",
                        items: [
                            new sap.m.Switch({
                                state: false
                            }),
                            new sap.m.Button({
                                text: "Info",
                                press: function(evt){
                                    createQinfo(evt, true);
                                }
                            })
                        ]
                    })
                ]
            })
        ]

    });
    var questionItem3 = new sap.m.CustomListItem({
        content: [
            new sap.m.FlexBox({
                direction: "Column",
                alignItems: "Start",
                items: [
                    new sap.m.Text({ text: "Rücken gerade"}),
                    new sap.m.FlexBox({
                        alignItems: "Start",
                        justifyContent: "SpaceBetween",
                        width: "100%",
                        items: [
                            new sap.m.Switch({
                                state: false
                            }),
                            new sap.m.Button({
                                text: "Info",
                                press: function(evt){
                                    createQinfo(evt, true);
                                }
                            })
                        ]
                    })
                ]
            })
        ]

    });

    var testDescItem = new sap.m.StandardListItem({
        title: "3/5 Butterfly",
        description: "Lorem Ipsum FooBaar undsoweiter",
        icon: "img/descImage.png",
        type: "Navigation",
        press: function(evt){
            createTestInfo();
        }
    });

    testDescList.removeAllItems();
    testDescList.addItem(testDescItem);
    questionList.addItem(questionItem1).addItem(questionItem2).addItem(questionItem3);
}

function createTestInfo(){
    app.to(pageTestInfo);
}

function createAuswertung(evt){
    app.to(pageAuswertung);

}

function createAuswDetail(evt){
    app.to(pageAuswDetail);
     var testDescItem2 = new sap.m.StandardListItem({
        title: "3/5 Butterfly",
        description: "Lorem Ipsum FooBaar undsoweiter",
        icon: "img/descImage.png",
        type: "Navigation",
        press: function(evt){
            createTestInfo();
        }
    });
    testDescList2.removeAllItems();
    testDescList2.addItem(testDescItem2);
}

function createQinfo(evt, withBtns){
    app.to(pageQuestionInfo);
    if (withBtns){
         qInfoToolbar.setVisible(true);
    } else {
        qInfoToolbar.setVisible(false);
    }
}

function createTrPlan(){
    app.to(pagePlan);
}

function toUebungInfo(evt){
    app.to(pageUebungInfo);
}

function toPageHistorie(evt){
    app.to(pageHistorie);
}

function toUebung(evt){
    app.to(pageUebung);
}




// -- PAGE MAIN ---




// function createKategories(){
//     klist.bindItems({
//                 path : "/tests",
//                 sorter : new sap.ui.model.Sorter("topId"),
//                 filters: new sap.ui.model.Filter("typ", sap.ui.model.FilterOperator.EQ, "g"),
//                 template : new sap.m.StandardListItem({
//                     title: "{titel}",
//                     description: "{beschreibung}",
//                     type: sap.m.ListType.Navigation,
//                     press: function(evt){ createTests(evt); }
//                 })
//     });
// }


// function createTests(evt){
//     gId = evt.getSource().getBindingContext().getProperty("id");
//     app.to(pageTests);
//     (app.getCurrentPage()).setTitle(evt.getSource().getBindingContext().getProperty("titel"));
//     loadTestItems(evt.getSource().getBindingContext().getProperty("id"));
// }


// function loadTestItems(testId){
//      tList.bindItems({
//         path : "/tests",
//         sorter : new sap.ui.model.Sorter("id"),
//         filters: [new sap.ui.model.Filter("typ", sap.ui.model.FilterOperator.EQ, "t"),
//                     new sap.ui.model.Filter("topId", sap.ui.model.FilterOperator.EQ, testId)],
//         template : new sap.m.StandardListItem({
//             title: "{titel}",
//             description: "{beschreibung}",
//             type: sap.m.ListType.Navigation,
//             press: function(evt){ createTest(evt, null); }
//         })
//     });
// }

// function createTest(evt, prevTestId, type){

//     app.to(pageTest);
//     if (prevTestId == null){
//          tId = evt.getSource().getBindingContext().getProperty("id");
//          mediaURL = evt.getSource().getBindingContext().getProperty("media");
//          title = evt.getSource().getBindingContext().getProperty("titel");
//      } else {
//         if (type == "prev"){
//             tId = --prevTestId;
//         } else {
//             tId = ++prevTestId;
//         }
//         mediaURL = _.result(_.find(sap.ui.getCore().getModel().getObject("/tests"), { 'id': tId}), "media");
//         title = _.result(_.find(sap.ui.getCore().getModel().getObject("/tests"), { 'id': tId}), "titel");
//      }
//      fillTestContent(title, mediaURL);
// }

// function fillTestContent(title, mediaURL){
//     currentPage = app.getCurrentPage();
//     imageTest.setSrc(mediaURL);
//     currentPage.setTitle(title);
//     panelTest.removeAllContent();
//     panelTest.insertContent(imageTest);
//     loadQuestions();
// }

// function loadQuestions(){
//     fPanel.insertContent(fList);
//     fList.bindItems({
//         path : "/frage",
//         sorter : new sap.ui.model.Sorter("id"),
//         filters: [new sap.ui.model.Filter("testId", sap.ui.model.FilterOperator.EQ, tId)],
//         template : new sap.m.CustomListItem({
//                 type: sap.m.ListType.Active,
//                 content: [
//                         new sap.m.FlexBox({
//                             alignItems: "Center",
//                             justifyContent: "SpaceBetween",
//                             items: [
//                                  new sap.m.Text({
//                                  text: "{text}",


//                                 }),
//                                  new sap.m.FlexBox({
//                                     items: [
//                                             new sap.m.Button({
//                                             type: "Reject",
//                                             press: function(evt) {
//                                                handleLeftButton(this, evt.getSource().getBindingContext().getProperty("id"));
//                                             }
//                                         }),

//                                             new sap.m.Button({
//                                             type: "Accept" ,
//                                             press: function(evt) {
//                                                 handleRightButton(this, evt.getSource().getBindingContext().getProperty("id"));

//                                             }
//                                         })
//                                     ]
//                                  })
//                             ]
//                         }),
//             ],
//             press: function(evt){
//                 createQuestion(evt);
//             }
//         })
//     });
//     updateIcons(tId);
// }

// function handleLeftButton(btn, pFrageId){
//      if (btn.getIcon() === sap.ui.core.IconPool.getIconURI("decline")){
//         switch (_.filter(auswertung["auswertung"], {id: pFrageId } ).length){
//             case 0:
//                 break;
//             case 1:
//                 _.remove(auswertung["auswertung"], function(n){
//                     return n.id == pFrageId;
//                 });
//                 break;
//             default:
//                 break;
//         }
//         btn.setIcon(null);
//     } else {
//         switch (_.filter(auswertung["auswertung"], {id: pFrageId} ).length){
//             case 0:
//                 auswertung["auswertung"].push({id: pFrageId, istBestanden: false});
//                 break;
//             case 1:
//                 _.findWhere(auswertung["auswertung"], {id: pFrageId} ).istBestanden = false;
//                 break;
//             default:
//                 break;
//         }
//         btn.getParent().getItems()[1].setIcon(null);
//         btn.setIcon(sap.ui.core.IconPool.getIconURI("decline"));
//     }
//     console.log(JSON.stringify(auswertung));
// }



// function handleRightButton(btn, pFrageId){
//      if (btn.getIcon() === sap.ui.core.IconPool.getIconURI("accept")){
//         switch (_.filter(auswertung["auswertung"], {id: pFrageId } ).length){
//             case 0:
//                 break;
//             case 1:
//                 _.remove(auswertung["auswertung"], function(n){
//                     return n.id == pFrageId;
//                 });
//                 break;
//             default:
//                 break;
//         }
//         btn.setIcon(null);
//     } else {
//         switch (_.filter(auswertung["auswertung"], {id: pFrageId} ).length){
//             case 0:
//                 auswertung["auswertung"].push({id: pFrageId, istBestanden: true});
//                 break;
//             case 1:
//                 _.findWhere(auswertung["auswertung"], {id: pFrageId} ).istBestanden = true;
//                 break;
//             default:
//                 break;
//         }
//         btn.getParent().getItems()[0].setIcon(null);
//         btn.setIcon(sap.ui.core.IconPool.getIconURI("accept"));
//     }
//     console.log(JSON.stringify(auswertung));
// }

// function updateIcons(testId){
//     console.log("updateIcons");
//     sap.ui.getCore().getModel("auswertung").setData(auswertung);
//     var questions = _.pluck(_.sortBy(_.filter(sap.ui.getCore().getModel().getData().frage, { 'testId': testId}), 'lfdnr' ), 'id');
//     for (i=0; i<questions.length; i++){
//        var bestanden = _.result(_.findWhere(sap.ui.getCore().getModel("auswertung").getData().auswertung, { 'id': questions[i] }), 'istBestanden');
//        if (bestanden != null) {
//            if (bestanden){
//                 fList.getItems()[i].getContent()[0].getItems()[1].getItems()[0].setIcon(null);
//                 fList.getItems()[i].getContent()[0].getItems()[1].getItems()[1].setIcon(sap.ui.core.IconPool.getIconURI("accept"));
//            } else {
//                 fList.getItems()[i].getContent()[0].getItems()[1].getItems()[0].setIcon(sap.ui.core.IconPool.getIconURI("decline"));
//                 fList.getItems()[i].getContent()[0].getItems()[1].getItems()[1].setIcon(null);
//            }
//        }
//     }
// }

// function createQuestion(evt){
//     app.to(pageQuestion);
//     console.log(evt.getSource().getBindingContext().getObject());
//     var frageId = evt.getSource().getBindingContext().getObject().id;
//     currentPage = app.getCurrentPage();
//     var questionText2 = new sap.m.Text({
//          text: {
//            parts: ['/langtext'],
//            formatter: function (langtext) {
//                         return _.pluck(_.sortBy(_.filter(langtext, { 'objId': frageId, type: 'f'}), 'lfdnr'), 'text');
//                         },
//          },
//     });
//     panelQuestion.removeAllContent();
//     panelQuestion.addContent(questionText2);
//     testIdInput.setValue(evt.getSource().getBindingContext().getProperty("testId"));
// }

// function nextPractise(btn, evt){
//     if(nextExists(tId)){
//         createTest(evt, tId, 'test');
//         auswBtn.setEnabled(false)
//     } else {
//         auswBtn.firePress();
//        auswBtn.setEnabled(true);
//     }
// }


// function prevPractise(evt){
//      if(prevExists(tId)){
//         createTest(evt, tId, 'prev');
//     } else {
//         sap.ui.commons.MessageBox.alert("Keine vorherige Übung vorhanden !");
//     }
// }


// function nextExists(tId){
//     var tests = sap.ui.getCore().getModel().getObject("/tests");
//     var currObj = _.findWhere(tests, {id: tId});
//     var count = _.size(_.pluck(_.filter(tests, { topId: currObj.topId, lfdnr: currObj.lfdnr+1 }), "id"));
//     if (count == 1)
//         return true;
//     return false;
// }

// function prevExists(tId){
//     var tests = sap.ui.getCore().getModel().getObject("/tests");
//     var currObj = _.findWhere(tests, {id: tId});
//     var count = 0;
//     if (currObj != null){
//         count = _.size(_.pluck(_.filter(tests, { topId: currObj.topId, lfdnr: currObj.lfdnr-1 }), "id"));
//     }
//     if (count == 1)
//         return true;
//     return false;
// }

// function frageInAuswertung(pfrageId){
//     var auswObj = sap.ui.getCore().getModel().getObject("/auswertung");
//     if (_.size(_.findWhere(auswObj, { frageId: pfrageId})) > 0)
//         return true;
//     return false;
// }

// // ----------------------------------------    DUMMY-ELEMENTE      -----------------------------------------

// function createUebung(evt){
//     //pageUebung.removeAllContent();
//     app.to(pageUebung);
//     var uebObj = evt.getSource().getBindingContext().getObject();
//     pageUebung.setTitle(uebObj.title);
//     mediaUeb.setSrc(uebObj.media);
//     panelTextUeb.setText(uebObj.descText);
//     var grade = sap.ui.getCore().getModel("globData").getData().grade;
//     var set, wdh, pause;
//     switch(grade){
//         case 1:
//             set = uebObj.a_set;
//             wdh = uebObj.a_wdh;
//             pause = uebObj.a_pause;
//             break;
//         case 2:
//             set = uebObj.f_set;
//             wdh = uebObj.f_wdh;
//             pause = uebObj.f_pause;
//             break;
//         case 3:
//             set = uebObj.e_set;
//             wdh = uebObj.e_wdh;
//             pause = uebObj.e_pause;
//             break;
//     }
//     condText.setText("Set: " +set+ ", Wdhlg: " +wdh+ ", Pause: " +pause);

// }


// function toPlan(evt){
//     app.to(pagePlan);
//     var planModel2 = new sap.ui.model.json.JSONModel();
//     var planObj = sap.ui.getCore().getModel("auswertung").getData().auswertung;
//     var planObj2 = { planOb : [] };
//     for (i = 0; i<planObj.length; i++){
//         if(planObj[i].istBestanden ===false){
//             var uObjs = _.filter(sap.ui.getCore().getModel().getObject("/uebung"), {frageId: planObj[i].id});
//             for (j = 0; j < _.size(uObjs); j++){
//                 planObj2['planOb'].push(uObjs[j]);
//             }
//         }
//     }
//     planModel2.setData(planObj2);
//     uList.setModel(planModel2);
//     uList.bindItems({
//         path: "/planOb",
//         sorter : new sap.ui.model.Sorter("gewichtung"),
//         template: new sap.m.StandardListItem({
//             type: sap.m.ListType.Navigation,
//             title: "{title}",
//             press: function(evt){
//                 createUebung(evt);
//             }
//         })
//     });
// }
//  var uebungenZurAntwort = null;

// function toAuswertung(evt){
//     app.to(pageAuswertung);
//     sap.ui.getCore().getModel("auswertung").setData(auswertung);
//     aList.bindItems({
//         path: "/tests",
//         sorter: new sap.ui.model.Sorter("lfdnr"),
//         filters: [new sap.ui.model.Filter("topId", sap.ui.model.FilterOperator.EQ, gId)],
//         template: new sap.m.DisplayListItem({
//             label: "{titel}",
//             value: {
//                 parts: ['/tests'],
//                 formatter: function (id, foo) {
//                     var result, id, bestanden, fragen;
//                     id = this.getModel().getObject(this.getBindingContext().sPath).id;
//                     fragen = _.filter(sap.ui.getCore().getModel().getObject("/frage"), { testId: id }),
//                     result = _.size(_.pluck(fragen, 'id'));
//                     bestanden = 0;
//                     for (i=0; i<result; i++){
//                         if(_.size(_.pluck(_.findWhere(sap.ui.getCore().getModel("auswertung").getData().auswertung, { id: fragen[i].id, istBestanden: true }))) > 0)
//                             bestanden++;
//                     }
//                     return bestanden + "/" + result;
//                 },
//             },
//             type: sap.m.ListType.Navigation,
//             press: function(evt){
//                toAuswDetails(evt.getSource().getBindingContext().getObject().id);
//             }
//         })
//     });
// }

// function toAuswDetails(testId){
//     panelAuswDetail.removeAllContent();
//     var fragenZumTest = _.sortBy(_.filter(sap.ui.getCore().getModel().getObject("/frage"), { 'testId': testId}), 'lfdnr');
//     for (var i=0; i<fragenZumTest.length; i++){
//         panelAuswDetail.addContent(
//             new sap.m.StandardListItem({
//                 title: i+1 +". Frage"
//             })
//         );

//         var istBestanden = _.result(_.findWhere(sap.ui.getCore().getModel("auswertung").getData().auswertung, {id: fragenZumTest[i].id}), 'istBestanden');

//         uebungenZurAntwort = _.filter(sap.ui.getCore().getModel().getObject("/uebung"), { frageId: fragenZumTest[i].id, 'istBestanden': istBestanden });
//         for (var j=0; j<_.size(_.pluck(uebungenZurAntwort)); j++){
//             panelAuswDetail.addContent(
//                 new sap.m.Text({
//                     text: uebungenZurAntwort[j].auswertungText
//                 })
//             );
//         }
//     }
//     app.to(pageAuswDetail);
// }




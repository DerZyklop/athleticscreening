

function successCallbackLastIndex(data, response){
    currentTestIndex = data.results.length - 1;
}


function successCallbackTestData(data, response){
    currentTests = data.results;
    var pageTestModel = new sap.ui.model.json.JSONModel();
    var currentTest = currentTests[currentTestIndex];
    pageTestModel.setData(currentTest);

    var testTitle = new sap.ui.core.HTML({
        content: "<p><b><font color='green'>" + currentTest.Lfdnr + "/" + currentTests.length + "</font> " + currentTest.Text + "</b></p><p>Mit dieser Übung können Sie ...<p>"
    });

    var bildUrl = "img/" + currentTest['Bild'].toLowerCase();

    var testImage = new sap.m.Image({
        src: bildUrl,
        height: "6em"
    });

    var testDescItem = new sap.m.CustomListItem({
        type: "Navigation",
        content: [
            new sap.m.FlexBox({
                alignment: "Start",
                width: "100%",
                items: [
                    testImage,
                    new sap.m.FlexBox({
                        direction: "Column",
                        alignItems: "Start",
                        width: "60%",
                        items: [
                            testTitle
                        ]
                    }).addStyleClass('flexMargin')
                ]
            }).addStyleClass('flexPadding')

        ],
        press: function(evt){
            createTestInfo();
        }
    });

    testDescList.addItem(testDescItem);
    getTestLangTextData(currentTest['Id'], 'main');
    getQuestionsData(currentTest['Id']);
    getTestMediaData(currentTest['Id']);
    var value = ((currentScreenIndex + 1) / selectedScreens.length) * 100;
    screeningProgressIndicator.setPercentValue(value);
    testProgressIndicator.setPercentValue((parseInt(currentTest.Lfdnr) / currentTests.length) * 100);
    var title = (currentScreenIndex + 1) + "/" + selectedScreens.length + " " + selectedScreens[currentScreenIndex].Text;
    pageTest.setTitle(title);
}

function successCallbackTestMediaData(data, response){
    var testMediaModel = new sap.ui.model.json.JSONModel();
    imgCarousel.setModel(testMediaModel);
    currentTestMedia = data.results;
    testMediaModel.setData(data.results);
}

function successCallbackUebungMediaData(data, response){
    var uebungMediaModel = new sap.ui.model.json.JSONModel();
    uebungCarousel.setModel(uebungMediaModel);
    currentUebungMedia = data.results;
    uebungMediaModel.setData(data.results);
}

function successCallbackUebungMediaDataNext(data, response){
    currentUebungMedia = data.results;
    uebungCarousel.getModel().setData(currentUebungMedia);

}

function successCallbackQData(data, response){
    currentQuestions = data.results;
    bindQuestions();
}

function successCallbackMenuData(data, response){
    var menuModel = new sap.ui.model.json.JSONModel();
    menuData = _.filter(data.results, { Genre: 'MENU' });
    var questionData = _.filter(data.results, { Genre: 'FRAGE' });
    //alert("qd: " + JSON.stringify(questionData));
    menuModel.setData(menuData);
    screeningData = data.results;
    screeningList.setModel(menuModel);
    trainingList.setModel(menuModel);

    screeningList.bindItems({
        path: "/",
        sorter: new sap.ui.model.Sorter("Lfdnr"),
        filters: [ new sap.ui.model.Filter("Genre", sap.ui.model.FilterOperator.EQ, "MENU")],
        template: new sap.m.CustomListItem({
            content: [
                new sap.m.FlexBox({
                    direction: "Column",
                    alignItems: "Start",
                    items: [
                        new sap.ui.core.HTML({content: "</br>"}),
                        new sap.m.Text({ text: "{Text}"}),
                        new sap.ui.core.HTML({content: "<hr>"}),
                        new sap.m.FlexBox({
                            alignItems: "Start",
                            justifyContent: "SpaceBetween",
                            width: "100%",
                            items: [
                                new sap.m.Switch({
                                    state: {
                                            parts: [
                                                {path: "/Id"},
                                                {path: "/Text"}
                                            ],
                                            formatter: function(Id, Text){
                                                if (contains(ScAuswahl, Id))
                                                    return true;
                                                return false;
                                            }
                                        },
                                    change: function(evt){
                                        var i;
                                        if (this.getState()){
                                           ScAuswahl.push(evt.getSource().getBindingContext().getProperty('Id'));
                                        } else {
                                            for (i = 0; i < ScAuswahl.length; i++){
                                                if(ScAuswahl[i] === evt.getSource().getBindingContext().getProperty('Id'))
                                                    delete ScAuswahl[i];

                                            }
                                        }
                                    }
                                }),
                                new sap.m.Button({
                                    width: "100%",
                                    text: {
                                        parts: [
                                            {path: "/Id"},
                                            {path: "/Text"}
                                        ],
                                        formatter: function(id, text){
                                            var anzahlBestanden = 0;
                                            var anzahlGesamt = 0;
                                            var sId = this.getBindingContext().getProperty('Id');
                                            var tests = _.filter(screeningData, { 'Topid': sId});
                                            for (j = 0; j < tests.length; j++){
                                                var fragen = _.filter(screeningData, {Topid: tests[j].Id});
                                                anzahlGesamt += fragen.length;
                                                for (f = 0; f < fragen.length; f++){
                                                    if (fragen[f].Bestanden === '1'){
                                                        anzahlBestanden++;
                                                    }
                                                }

                                            }
                                            return anzahlBestanden + "/" + anzahlGesamt;
                                        }
                                    },
                                    press: function(evt){
                                        createAuswertung(evt);
                                    }
                                })

                            ]
                        }),
                        new sap.ui.core.HTML({content: "</br>"})
                    ]
                })

            ]
        })
    });
    trainingList.bindItems({
                path: "/",
                sorter: new sap.ui.model.Sorter("Lfdnr"),
                filters: [ new sap.ui.model.Filter("Genre", sap.ui.model.FilterOperator.EQ, "MENU")],
                template: new sap.m.CustomListItem({
                    content: [
                        new sap.m.FlexBox({
                            direction: "Column",
                            alignItems: "Start",
                            items: [
                                new sap.ui.core.HTML({content: "</br>"}),
                                new sap.m.Text({ text: "{Text}"}),
                                new sap.ui.core.HTML({content: "<hr>"}),
                                new sap.m.FlexBox({
                                    alignItems: "Start",
                                    justifyContent: "SpaceBetween",
                                    width: "100%",
                                    items: [
                                        new sap.m.Switch({
                                            state: {
                                                parts: [
                                                    {path: "/Id"},
                                                    {path: "/Text"}
                                                ],
                                                formatter: function(Id, Text){
                                                    if (contains(TrAuswahl, Id))
                                                        return true;
                                                    return false;
                                                }
                                            },
                                            change: function(evt){
                                                var i;
                                                if (this.getState()){
                                                    if(!contains(TrAuswahl, evt.getSource().getBindingContext().getProperty('Id')))
                                                        TrAuswahl.push(evt.getSource().getBindingContext().getProperty('Id'));
                                                } else {
                                                    for (i = 0; i < TrAuswahl.length; i++){
                                                        if(TrAuswahl[i] === evt.getSource().getBindingContext().getProperty('Id')){
                                                            delete TrAuswahl[i];
                                                        }

                                                    }
                                                }
                                            }
                                        }),
                                        new sap.m.Button({
                                            width: "100%",
                                            text: {
                                            parts: [
                                                {path: "/Id"},
                                                {path: "/Text"}
                                            ],
                                            formatter: function(id, text){
                                                var anzahlBestanden = 0;
                                                var anzahlGesamt = 0;
                                                var sId = this.getBindingContext().getProperty('Id');
                                                //alert("id: " + sId + "; data.length: " + screeningData.length);
                                                var tests = _.filter(screeningData, { 'Topid': sId});
                                                //alert("tests: " + tests.length);
                                                for (j = 0; j < tests.length; j++){
                                                    var fragen = _.filter(screeningData, {Topid: tests[j].Id});
                                                    //alert("fragen: " + fragen.length);
                                                    anzahlGesamt += fragen.length;
                                                    for (f = 0; f < fragen.length; f++){
                                                        if (fragen[f].Bestanden === '1'){
                                                            anzahlBestanden++;
                                                        }
                                                    }
                                                }
                                                return anzahlBestanden + "/" + anzahlGesamt;
                                            }
                                        },
                                            press: function(evt){
                                                createAuswertung(evt);
                                            }
                                        })
                                    ]
                                }),
                                new sap.ui.core.HTML({content: "</br>"})
                            ]
                        })
                    ]
                })
             });


}

function successCallbackTLangMain(data, response){
}

function successCallbackTLangInfo(data, response){
    var htmlText = "";
    var i;
    for (i = 0; i < data.results.length; i++){
        htmlText += data.results[i].Text;
    }
    testDetailDesc.setContent("<div>" + htmlText + "</div>");
}

function successCallbackQInfo(data, response){
    var htmlText = "<div class='panelMargin'>";
    var i;
    for (i = 0; i < data.results.length; i++){
        htmlText += data.results[i].Text;
    }
    htmlText += "</div>";
    qInfoText.setContent(htmlText);
}

function successCallbackPlanData(data, response){
    createTrPlan(data.results);
}

function successCallbackLevelData(data, response){
    levelData = _.findWhere(data.results, { 'Niveau': level });
    uebungTitle.setContent("<h3><font color='green'>" + (currSatz + 1) + "/" + levelData.Satz + "</font> " + app.getCurrentPage().getModel().getObject('/Text') + "</h3>");
    bindUebItems();
}

function successCallbackLoadLevelData(data, response){
    planLevelData = data.results;
}

function successCallbackScreeningData(data, response){
    screeningData = data.results;
}

function successCallbackUebungMediaData2(data, response){
    var uCarouselModel = new sap.ui.model.json.JSONModel();
    uCarousel.setModel(uCarouselModel);
    uCarouselModel.setData(data.results);
}

function successCallbackUebungLangTextData(data, response){
    //alert("Langtext: " + JSON.stringify(data.results));
    var i;
    var htmlText = "";
    for (i = 0; i < data.results.length; i++){
        htmlText += data.results[i].Text;
    }
    uDetailDesc.setContent("<div>" + htmlText + "</div>");

}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------

function getPlanData(auswahl){
    var oHeaders = {};
    var url = '';
    oHeaders['Authorization'] = auth;
    oHeaders['X-SMP-APPCID'] = applicationContext.applicationConnectionId;
    url += applicationContext.applicationEndpointURL + "/ZSCREENINGSet";
    url += "?$format=json'";
    var request = {
        'headers': oHeaders,
        'requestUri': url,
        'method': 'GET'
    };
    OData.read(request, successCallbackPlanData, errorCallback);
}


function getTestMediaData(testId){
    var oHeaders = {};
    var url = '';
    oHeaders['Authorization'] = auth;
    oHeaders['X-SMP-APPCID'] = applicationContext.applicationConnectionId;
    url += applicationContext.applicationEndpointURL + "/ZMEDIASet";
    url += "?$format=json&$filter=Screeningid eq '" + testId + "'&$orderby=Lfdnr";
    var request = {
        'headers': oHeaders,
        'requestUri': url,
        'method': 'GET'
    };
    OData.read(request, successCallbackTestMediaData, errorCallback);
}

function getUebungMediaData(uebungId, isNext){
    var oHeaders = {};
    var url = '';
    oHeaders['Authorization'] = auth;
    oHeaders['X-SMP-APPCID'] = applicationContext.applicationConnectionId;
    url += applicationContext.applicationEndpointURL + "/ZMEDIASet";
    url += "?$format=json&$filter=Screeningid eq '" + uebungId + "'&$orderby=Lfdnr";
    var request = {
        'headers': oHeaders,
        'requestUri': url,
        'method': 'GET'
    };
    if(isNext){
        // alert("next");
         OData.read(request, successCallbackUebungMediaDataNext, errorCallback);
    } else {
        // alert("!next");
        OData.read(request, successCallbackUebungMediaData, errorCallback);
    }
}

function getTestsData(menuId){
    var oHeaders = {};
    var url = '';
    oHeaders['Authorization'] = auth;
    oHeaders['X-SMP-APPCID'] = applicationContext.applicationConnectionId;
    url += applicationContext.applicationEndpointURL + "/ZSCREENINGSet";
    url += "?$format=json&$filter=Topid eq '" + menuId + "'&$orderby=Lfdnr";
    var request = {
        'headers': oHeaders,
        'requestUri': url,
        'method': 'GET'
    };
    OData.read(request, successCallbackTestData, errorCallback);
}

function getQuestionsData(testId){
    var oHeaders = {};
    var url = '';
    oHeaders['Authorization'] = auth;
    oHeaders['X-SMP-APPCID'] = applicationContext.applicationConnectionId;
    url += applicationContext.applicationEndpointURL + "/ZSCREENINGSet";
    url += "?$format=json&$filter=Topid eq '" + testId + "'&$orderby=Lfdnr";
    var request = {
        'headers': oHeaders,
        'requestUri': url,
        'method': 'GET'
    };
    OData.read(request, successCallbackQData, errorCallback);
}

function getMenuData(){
    var oHeaders = {};
    var url = '';
    oHeaders['Authorization'] = auth;
    oHeaders['X-SMP-APPCID'] = applicationContext.applicationConnectionId;
    url += applicationContext.applicationEndpointURL + "/ZSCREENINGSet";
    url += "?$format=json";
    var request = {
        'headers': oHeaders,
        'requestUri': url,
        'method': 'GET'
    };
    OData.read(request, successCallbackMenuData, errorCallback);
}

function setIndexToLast(menuId){
    var oHeaders = {};
    var url = '';
    oHeaders['Authorization'] = auth;
    oHeaders['X-SMP-APPCID'] = applicationContext.applicationConnectionId;
    url += applicationContext.applicationEndpointURL + "/ZSCREENINGSet";
    url += "?$format=json&$filter=Topid eq '" + menuId + "'&$orderby=Lfdnr";
   // url += "?$format=json";
    var request = {
        'headers': oHeaders,
        'requestUri': url,
        'method': 'GET'
    };
        OData.read(request, successCallbackLastIndex, errorCallback);
}

function putAnswer(question, answer){
        var answerNew = 0;
        if (answer){
            answerNew = 1;
        } else if (!answer){
            answerNew = 0;
        }
        var url = applicationContext.applicationEndpointURL + "/ZSCREENINGSet('" + question.Id + "')";
        var oHeaders = {};
        var params = {};
        oHeaders['Authorization'] = auth;
        params.Bestanden = answerNew;
        params.Text = question.Text;
        params.Topid = question.Topid;
        params.Lfdnr = question.Lfdnr;
        params.Genre = question.Genre;
        params.Gewichtung = question.Gewichtung;
        var request = {
            'headers': oHeaders,
            'requestUri': url,
            'method': "PUT",  //merge not supported for this OData producer
            'data': params
        };
        OData.request(request, successCallBack, errorCallback);
}

function successCallBack(data, response){
     getMenuData();
}

function putAnswers(){
    var i;
    for (i = 0; i < currentQuestions.length; i++){
       if (questionStates['' + currentQuestions[i].Id + ''] != null){
            putAnswer(currentQuestions[i], questionStates['' + currentQuestions[i].Id + '']);
       }
    }
    questionStates = {};
}

function getTestLangTextData(testId, place){
    var oHeaders = {};
    var url = '';
    oHeaders['Authorization'] = auth;
    oHeaders['X-SMP-APPCID'] = applicationContext.applicationConnectionId;
    url += applicationContext.applicationEndpointURL + "/ZLANGTEXTSet";
    url += "?$format=json&$filter=Screeningid eq '" + testId + "'&$orderby=Lfdnr";
    var request = {
        'headers': oHeaders,
        'requestUri': url,
        'method': 'GET'
    };
    if (place === "main"){
         OData.read(request, successCallbackTLangMain, errorCallback);
     } else {
         OData.read(request, successCallbackTLangInfo, errorCallback);
     }
}

function getQInfoData(qId){
    var oHeaders = {};
    var url = '';
    oHeaders['Authorization'] = auth;
    oHeaders['X-SMP-APPCID'] = applicationContext.applicationConnectionId;
    url += applicationContext.applicationEndpointURL + "/ZLANGTEXTSet";
    url += "?$format=json&$filter=Screeningid eq '" + qId + "'&$orderby=Lfdnr";
   // url += "?$format=json";
    var request = {
        'headers': oHeaders,
        'requestUri': url,
        'method': 'GET'
    };
    OData.read(request, successCallbackQInfo, errorCallback);
}

function getLevelData(uId, level_){
    level = level_;
    var oHeaders = {};
    var url = '';
    oHeaders['Authorization'] = auth;
    oHeaders['X-SMP-APPCID'] = applicationContext.applicationConnectionId;
    url += applicationContext.applicationEndpointURL + "/ZLEVELSet";
    url += "?$format=json&$filter=Screeningid eq '" + uId + "'";
    var request = {
        'headers': oHeaders,
        'requestUri': url,
        'method': 'GET'
    };
    OData.read(request, successCallbackLevelData, errorCallback);
}

function loadLevelData(){
    var oHeaders = {};
    var url = '';
    oHeaders['Authorization'] = auth;
    oHeaders['X-SMP-APPCID'] = applicationContext.applicationConnectionId;
    url += applicationContext.applicationEndpointURL + "/ZLEVELSet";
    url += "?$format=json&$filter=Niveau eq " + level;
    var request = {
        'headers': oHeaders,
        'requestUri': url,
        'method': 'GET'
    };
    OData.read(request, successCallbackLoadLevelData, errorCallback);
}

function getScreeningData(){
    var oHeaders = {};
    var url = '';
    oHeaders['Authorization'] = auth;
    oHeaders['X-SMP-APPCID'] = applicationContext.applicationConnectionId;
    url += applicationContext.applicationEndpointURL + "/ZSCREENINGSet";
    url += "?$format=json";
    var request = {
        'headers': oHeaders,
        'requestUri': url,
        'method': 'GET'
    };
    OData.read(request, successCallbackScreeningData, errorCallback);
}

function getUebungMediaData2(uId){
    var oHeaders = {};
    var url = '';
    oHeaders['Authorization'] = auth;
    oHeaders['X-SMP-APPCID'] = applicationContext.applicationConnectionId;
    url += applicationContext.applicationEndpointURL + "/ZMEDIASet";
    url += "?$format=json&$filter=Screeningid eq '" + uId + "'&$orderby=Lfdnr";
    var request = {
        'headers': oHeaders,
        'requestUri': url,
        'method': 'GET'
    };
    OData.read(request, successCallbackUebungMediaData2, errorCallback);
}

function getUebungLangTextData(uId){
    var oHeaders = {};
    var url = '';
    oHeaders['Authorization'] = auth;
    oHeaders['X-SMP-APPCID'] = applicationContext.applicationConnectionId;
    url += applicationContext.applicationEndpointURL + "/ZLANGTEXTSet";
    url += "?$format=json&$filter=Screeningid eq '" + uId + "'&$orderby=Lfdnr";
    var request = {
        'headers': oHeaders,
        'requestUri': url,
        'method': 'GET'
    };
    OData.read(request, successCallbackUebungLangTextData, errorCallback);
}


var toggleTimeCircles = function(el){
  if ( el.data('stopwatchIsRunning') ) {
    el.TimeCircles().stop();
    el.data('stopwatchIsRunning', false);
  } else {
    el.TimeCircles().start();
    el.data('stopwatchIsRunning', true);
  }

  el.TimeCircles().start();
};


var startWatch = function(el, sec, color){
  if (!sec) { sec = 60; }
  if (!color) { color = '#dd7733'; }
  el.data('timer', sec+1);
  el.TimeCircles({
      count_past_zero: false,
      circle_bg_color: '#ccc',
      total_duration: sec+1,
      time: {
        Days: {show: false},
        Hours: {show: false},
        Minutes: {show: false},
        Seconds: {text: "Sekunden", color: color}
      }
    }).rebuild().restart();
};




var stopwatchPlaceholder = new sap.ui.core.HTML('stopwatch', {
  content: '<div></div><button class="start">Start</button> <button class="stop">Stop</button><button class="restart">Restart</button>'
});

var pageStopwatch = new sap.m.Page("pageStopwatch", {
  title: "pageStopwatch",
  backgroundDesign: "Solid",
  showNavButton: true,
  navButtonPress: function(){
    app.back();
  },
  content: [
    new sap.ui.core.HTML({
      content: "<h2 style='margin: 20px'>Was ist Athletik Screening?</h2> "
    })
    //stopwatchPlaceholder
  ]
});




// pageStopwatch.addEventDelegate({
pageUebung.addEventDelegate({


  onAfterRendering: function(){
    var html = $('#stopwatch');
    html.css("width", '200px');
    html.css("height", '200px');
    html.css('margin', '0 auto');
    startWatch(html, 4);
    html.TimeCircles().addListener(function(unit, value, total){
      if (total === 0) {
        html.data('timer', '25');
        setTimeout(function(){
          startWatch(html, 26, '#009de0');
        }, 1000);
      }
    });

    html.data("stopwatchIsRunning", true);

    $(".start").click(function(){
      html.TimeCircles().start();
      html.data("stopwatchIsRunning", true);
    });
    $(".stop").click(function(){
      html.TimeCircles().stop();
      html.data("stopwatchIsRunning", false);
    });
    $(".restart").click(function(){
      html.TimeCircles().restart();
      html.data('stopwatchIsRunning', true);
    });
  },
  onAfterShow: function(){
    console.log('##onAfterShow##');

    var html = $('#stopwatch');

    html.click(function(){
      toggleTimeCircles(html);
    });

    html.TimeCircles().rebuild();
    setTimeout(function(){
      $('.example').TimeCircles().rebuild();
    }, 1000);
  }

});



// ---------- GLOBAL VARS -----------

var ScAuswahl = [];
var TrAuswahl = [];
var currentScreenIndex = 0;
var currentTestIndex = 0;
var screenArray;
var planArray;
var currentTests = [];
var currentQuestion;
var questionStates = {};
var currentTestDesc;
var currentSwitch;
var datenAuswahl1 = [];
var datenAuswahl2 = [];
var currentTestMedia = [];
var currentUebungMedia = [];
var menuData = [];
var line;
var selectedScreens = [];
var selectedPlan = [];
var switchItem;
var currentUebungIndex = 0;
var currentUebungKatIndex = 0;
var satz = 3;
var wdh = 5;
var currSatz = 0;
var currWdh = 0;
var level = 1;
var levelData;
var planLevelData;
var currentUebung;
var screeningData;

var html = new sap.ui.core.HTML({
    content: "<h2 style='color:green; text-align:center' >Willkommen</br> zur <b>Athletik Screening</b> App</h2>"
});

// ------------------------------------

function createMenu(evt){
}

// ----- WELCOME-PAGE ----- WELCOME-PAGE ----- WELCOME-PAGE ----- WELCOME-PAGE ----- WELCOME-PAGE ----

var html = new sap.ui.core.HTML({
    content: "<h2 style='color:green; text-align:center' >Willkommen</br> zur <b>Athletik Screening</b> App</h2>"
});


var welcomeItemList = new sap.m.List({
    items: [

        new sap.m.StandardListItem({
            type: sap.m.ListType.Navigation,
            title: "Testseite für die Stopwatch",
            press: function(evt){
                app.to(pageStopwatch);
            }
        }),
        new sap.m.StandardListItem({
            type: sap.m.ListType.Navigation,
            title: "Was ist Athletik Screening",
            press: function(evt){
                app.to(pageAbout);
            }
        }),
        new sap.m.StandardListItem({
            type: sap.m.ListType.Navigation,
            title: "Athletik Camp Website"
        }),
        new sap.m.StandardListItem({
            type: sap.m.ListType.Navigation,
            title: "Hauptmenü",
            press: function(evt){
                app.to(pageMenu);
                getMenuData();
                loadLevelData();
            }
        })
    ]
});

function resetListItemsState(){
    for (var i = 0; i < screeningList.getItems().length; i++){
            screeningList.getItems()[i].getContent()[0].getItems()[3].getItems()[0].setState(false);
            trainingList.getItems()[i].getContent()[0].getItems()[3].getItems()[0].setState(false);
    }
}

function menuVarInit(){
    datenAuswahl1 = [];
    datenAuswahl2 = [];
    currentTests = [];
    ScAuswahl = [];
    TrAuswahl = [];
    currentScreenIndex = 0;
    currentTestIndex = 0;
    screenArray = null;
    planArray = null;
    currentQuestion = null;
    questionStates = {};
    currentTestDesc = null;
    currentSwitch = null;
    currentTestMedia = [];
    currentUebungMedia = [];
    selectedScreens = [];
    switchItem = null;
    selectedPlan = [];
    currentUebungIndex = 0;
    currentUebungKatIndex = 0;
    currSatz = 0;
    resetListItemsState();
}

var screeningList = new sap.m.List({ width: "100%"});
var trainingList = new sap.m.List();


// ---------------- PAGE-TEST -------------------- ---------------- PAGE-TEST -------------------- ---------------- PAGE-TEST --------------------

var testProgressIndicator = new sap.m.ProgressIndicator({
    height: "10px",
    widht: "100%",
    percentValue: 33
}).addStyleClass('noMargin');



var screeningProgressIndicator = new sap.m.ProgressIndicator({
    height: "10px",
    widht: "100%",
    percentValue: 33
}).addStyleClass('noMargin');

var testDescList = new sap.m.List();

var questionList = new sap.m.List();
function doNothing(){

}
var testToolbar = new sap.m.Toolbar({
    content: [
        new sap.m.Button({
            text: "Zurück",
            icon: sap.ui.core.IconPool.getIconURI("arrow-left"),
            iconFirst: true,
            press: function(evt){
                putAnswers();
                //setTimeout(doNothing, 5000);
                imgCarousel.destroyPages();
                if(currentTestIndex > 0){
                    currentTestIndex--;
                    bindTestItems();
                } else {
                    if (currentScreenIndex > 0){
                        currentScreenIndex--;
                        setIndexToLast(selectedScreens[currentScreenIndex].Id);
                        bindTestItems();
                    } else {
                        app.backToPage(pageMenu);
                        getMenuData();
                        menuVarInit();
                    }
                }
            }
        }),
        new sap.m.ToolbarSpacer(),
        new sap.m.Button({
            text: "Weiter",
            icon: sap.ui.core.IconPool.getIconURI("arrow-right"),
            iconFirst: false,
            press: function(evt){
                putAnswers();
                imgCarousel.destroyPages();
                if(currentTestIndex < currentTests.length - 1){
                    currentTestIndex++;
                    bindTestItems();
                } else {
                    if (selectedScreens.length > currentScreenIndex + 1){
                        currentScreenIndex++;
                        screeningProgressIndicator.setPercentValue((currentScreenIndex + 1 / selectedScreens.length) * 100);
                        currentTestIndex = 0;
                        bindTestItems();
                    } else {
                        app.backToPage(pageMenu);

                        menuVarInit();
                    }
                }
            }
        })
    ]
});


// ----- ABOUT-PAGE ----- ABOUT-PAGE ----- ABOUT-PAGE ----- ABOUT-PAGE ----- ABOUT-PAGE ----- ABOUT-PAGE
// -------------------------------------------------------------------------------------------------------------------------------------------------


// ------------------------ NAVIGATION -------------------------
var planPanel = new sap.m.Panel({
    width: "100%"
});

function createTrPlan(daten){
    selectedPlan = [];
    var uebungen = [];
    var uebGefiltert = [];
    var j, f, i, p;
    for (i = 0; i < planArray.length; i++) {
        var menuItem = _.findWhere(menuData, { 'Id': planArray[i] });
        selectedPlan.push(menuItem);
    }
    selectedPlan = _.sortBy(selectedPlan, 'Lfdnr');
    app.to(pagePlan);
    for (i = 0; i < selectedPlan.length; i++){
        var text = new sap.m.Text({ text: selectedPlan[i].Text });
        var tests = _.filter(daten, { Topid: selectedPlan[i].Id});
        for (j = 0; j < tests.length; j++){
            var fragen = _.filter(daten, {Topid: tests[j].Id});
            for (f = 0; f < fragen.length; f++){
                var uebung;
                if (fragen[f].Bestanden === '1'){
                    uebGefiltert = _.filter(daten, { Topid: fragen[f].Id, Bestanden: '1'});
                    if (uebGefiltert.length > 0)
                        uebungen.push(uebGefiltert);
                }
                if (fragen[f].Bestanden === '0'){
                    uebGefiltert = _.filter(daten, { Topid: fragen[f].Id, Bestanden: '0'});
                    if (uebGefiltert.length > 0)
                        for (p = 0; p < uebGefiltert.length; p++)
                            uebungen.push(uebGefiltert[p]);
                    uebGefiltert = _.filter(daten, { Topid: fragen[f].Id, Bestanden: ''});
                    if (uebGefiltert.length > 0)
                        for (p = 0; p < uebGefiltert.length; p++)
                            uebungen.push(uebGefiltert[p]);
                }
            }

        }
        var liste = new sap.m.List();
        var listeModel = new sap.ui.model.json.JSONModel();
        liste.setModel(listeModel);
        listeModel.setData(uebungen);
        liste.bindItems({
            path: "/",
            //sorter: new sap.ui.model.Sorter("Gewichtung"),
            template: new sap.m.StandardListItem({
                type: "Navigation",
                title: "{Text}",
                description: {
                    parts: [
                            {path: 'Id'},
                            {path: 'Text'}
                        ],
                        formatter: function(id, text){
                            var levData = _.findWhere(planLevelData, { 'Screeningid': id });
                            return levData.Satz + " x " + levData.Wdh + " Wiederholungen";
                        }
                },
                press: function(evt){
                    toUebungInfo(evt);
                }
            })
        });

        pagePlan.addContent( text ).addContent( liste );
        selectedPlan[i].uebungen = uebungen;
        uebungen = [];
    }
}

function crScreening(){
    app.to(pagePlan);
}

function toUebungInfo(evt, text){
    var uebungInfoModel = new sap.ui.model.json.JSONModel();
    pageUebungInfo.setModel(uebungInfoModel);
    pageUebungInfo.setTitle(evt.getSource().getBindingContext().getProperty('Text'));
    uInfoPanel.setHeaderText("");
    getUebungMediaData2(evt.getSource().getBindingContext().getProperty('Id'));
    getUebungLangTextData(evt.getSource().getBindingContext().getProperty('Id'));
    app.to(pageUebungInfo);
}

function toPageHistorie(evt){
    app.to(pageHistorie);
}

function toUebung(evt){
    app.to(pageUebung);
    pageUebung.setTitle((currentUebungKatIndex + 1) + "/" + selectedPlan.length + " " + selectedPlan[currentUebungKatIndex].Text);
    var uebungModel = new sap.ui.model.json.JSONModel();
    var currentUebung = selectedPlan[currentUebungKatIndex].uebungen[currentUebungIndex];
    getUebungMediaData(currentUebung.Id);
    getLevelData(currentUebung.Id, level);
    pageUebung.setModel(uebungModel);
    uebungModel.setData(currentUebung);
    levelData = _.findWhere(planLevelData, { 'Screeningid': currentUebung.Id });
    uebungTitle.setContent("<h3><font color='green'>" + (currSatz + 1) + "/" + levelData.Satz + "</font> " + app.getCurrentPage().getModel().getObject('/Text') + "</h3>");
    uProgBar1.setPercentValue(((currentUebungKatIndex + 1) / selectedPlan.length) * 100);
    uProgBar2.setPercentValue(((currentUebungIndex + 1) / selectedPlan[currentUebungKatIndex].uebungen.length) * 100);

}

questionList.bindItems({
        path: "/",
        sorter: new sap.ui.model.Sorter("Lfdnr"),
        template: new sap.m.CustomListItem({
            content: [
                new sap.m.FlexBox({
                    direction: "Column",
                    alignItems: "Start",
                    items: [
                        new sap.ui.core.HTML({ content: "<p><font size='4'>{Text}</font></p>"}),
                        new sap.m.FlexBox({
                            alignItems: "Start",
                            justifyContent: "SpaceBetween",
                            width: "100%",
                            items: [
                                new sap.m.Switch({
                                    type: "AcceptReject",
                                    state: {
                                        parts: [
                                            {path: 'Bestanden'},
                                            {path: 'Text'},
                                            {path: 'Lfdnr'}
                                        ],
                                        formatter: function(bestanden, title, lfdnr){
                                            if(bestanden === "1"){
                                                this.getParent().getParent().addStyleClass('bestanden');
                                                return true;

                                            } else {
                                                this.getParent().getParent().addStyleClass('nBestanden');
                                                return false;
                                            }
                                        }
                                    },
                                    change: function(evt){
                                        questionStates['' + (evt.getSource().getBindingContext().getProperty('Id')) + ''] = this.getState();
                                        if (this.getState()){
                                            this.getParent().getParent().removeStyleClass('nBestanden');
                                            this.getParent().getParent().addStyleClass('bestanden');
                                        } else {
                                            this.getParent().getParent().removeStyleClass('bestanden');
                                            this.getParent().getParent().addStyleClass('nBestanden');
                                        }

                                    }
                                }),
                                new sap.m.Button({
                                    customData: [
                                        new sap.ui.core.CustomData({
                                            key: "hidden",
                                            value: "yes"
                                        })
                                    ],
                                    text: "Info",
                                    press: function(evt){
                                        currentQuestion = evt.getSource().getBindingContext().getProperty('Id');
                                        createQinfo(evt, true, (this.getParent()).getItems()[0]);
                                        toggle = (this.getParent()).getItems()[0];
                                        getQInfoData(evt.getSource().getBindingContext().getProperty('Id'));
                                    }
                                })
                            ]
                        })
                    ]
                })
            ]
        })
    });

function bindTestItems(){
    testDescList.removeAllItems();
    getTestsData(selectedScreens[currentScreenIndex].Id);
}

function bindQuestions(){
    var questionModel = new sap.ui.model.json.JSONModel();
    questionList.setModel(questionModel);
    questionModel.setData(currentQuestions);
}

function createScreening(daten){
    screenArray = daten;
    selectedScreens = [];
    var i;
    for (i = 0; i < screenArray.length; i++) {
        var menuItem = _.findWhere(menuData, { 'Id': daten[i] });
        selectedScreens.push(menuItem);
    }
    selectedScreens = _.sortBy(selectedScreens, 'Lfdnr');
    bindTestItems(daten);
    app.to(pageTest);
}

function contains(a, obj) {
    for (var i = 0; i < a.length; i++) {
        if (a[i] === obj) {
            return true;
        }
    }
    return false;
}




// ---------------- PAGE-MENU --------------------  ---------------- PAGE-MENU --------------------  ---------------- PAGE-MENU --------------------



var modus = null;
var tabMenu = new sap.m.IconTabBar("tabMenu", {
    items: [
        new sap.m.IconTabFilter("screeningTab", {
            text: "Screening",
            content: screeningList

        }),
        new sap.m.IconTabFilter("trainingTab", {
            text: "Trainingsplan",
            content: trainingList
        })
    ]
});

var menuToolbar = new sap.m.Toolbar({
    content: [
        new sap.ui.core.Icon({
            src: sap.ui.core.IconPool.getIconURI("settings")
        }),
        new sap.m.ToolbarSpacer(),
        new sap.m.Button({
            text: "Beginnen",
            press: function(evt){
                var i;
                for (i = 0; i < ScAuswahl.length; i++){
                    if(ScAuswahl[i] != null){
                        if (!contains(datenAuswahl1, ScAuswahl[i]))
                            datenAuswahl1.push(ScAuswahl[i]);
                    }
                }
                for (i = 0; i < TrAuswahl.length; i++){
                    if(TrAuswahl[i] != null){
                        if (!contains(datenAuswahl2, TrAuswahl[i]))
                            datenAuswahl2.push(TrAuswahl[i]);
                    }
                }

                switch((app.getCurrentPage().getContent()[0]).getSelectedKey()){
                    case "screeningTab":
                        if (datenAuswahl1.length > 0 ){
                            console.log("crScreening");
                            createScreening(datenAuswahl1);
                        }
                        break;
                    case "trainingTab":
                        if (datenAuswahl2.length > 0 ){
                            getPlanData(datenAuswahl2);
                            planArray = datenAuswahl2;
                        }
                        break;
                }

            }
        })
    ]
});



// ------------ PAGE-QUENSTIONINFO ------------- ------------ PAGE-QUENSTIONINFO ------------- ------------ PAGE-QUENSTIONINFO -------------
var qInfoText = new sap.ui.core.HTML();

var qJa = new sap.m.Button({
            text: "JA",
            width: "50%",
            type: "Accept",
            press: function(evt){
                questionStates['' + (currentQuestion) + ''] = true;
                switchItem.setState(true);
                switchItem.getParent().getParent().removeStyleClass('nBestanden');
                switchItem.getParent().getParent().addStyleClass('bestanden');
                //bindTestItems();
                app.backToPage(pageTest);

            }
        });
var qNein = new sap.m.Button({
            text: "NEIN",
            width: "50%",
            type: "Reject",
            press: function(evt){
                questionStates['' + currentQuestion + ''] = false;
                switchItem.setState(false);
                switchItem.getParent().getParent().removeStyleClass('bestanden');
                switchItem.getParent().getParent().addStyleClass('nBestanden');
                //bindTestItems();
                app.backToPage(pageTest);
            }
        });

var qInfoToolbar = new sap.m.Toolbar({
    content: [
        qJa,
        new sap.m.ToolbarSpacer(),
        qNein
    ]
});


// ---------------- PAGE-TESTINFO ---------------- ---------------- PAGE-TESTINFO ---------------- ---------------- PAGE-TESTINFO ----------------
var imgCarousel = new sap.m.Carousel({
    height: "20%"
});

imgCarousel.bindAggregation("pages", {
    path: "/",
    sorter: new sap.ui.model.Sorter("Lfdnr"),
    template: new sap.m.Image({
        height: "200px",
        src: {
            parts: [
                { path: "Url"},
                { path: "Lfdnr"}
            ],
            formatter: function(url, lfdnr){
                return "img/" + url.toLowerCase();
            }
        }
    })
});

var testDetailDesc = new sap.ui.core.HTML({
    content: []
});

var testInfoPanel = new sap.m.Panel({
    headerText: "Butterfly",
    content: [
        imgCarousel, testDetailDesc
    ]
}).addStyleClass('flexPadding');



// ----------------- PAGE-AUSWERTUNG ------------- ----------------- PAGE-AUSWERTUNG ------------- ----------------- PAGE-AUSWERTUNG -------------

var auswertungList = new sap.m.List({
    width: "100%",
    items: [
        new sap.m.CustomListItem({
            width: "100%",
            type: "Navigation",
            content: [
                new sap.m.ProgressIndicator({

                    displayValue: "3/5 Butterfly",
                    percentValue: 60.0,
                    state: "Success"
                })
            ],
            press: function(evt){
                createAuswDetail(evt);
            }
        }),
        new sap.m.CustomListItem({
            type: "Navigation",
            content: [
                new sap.m.ProgressIndicator({

                    displayValue: "5/5 Push Ups",
                    percentValue: 100.0,
                    state: "Success"
                })
            ],
            press: function(evt){
                createAuswDetail(evt);
            }
        }),
        new sap.m.CustomListItem({
            type: "Navigation",
            content: [
                new sap.m.ProgressIndicator({

                    displayValue: "2/5 Hantelstange",
                    percentValue: 40.0,
                    state: "Success"
                })
            ],
            press: function(evt){
                createAuswDetail(evt);
            }
        })
    ]
});

var progressbarBar = new sap.m.Toolbar({
    content: [
        new sap.m.ProgressIndicator({
            displayValue: "8/15 Gesamt",
            percentValue: 53.0,
            state: "Success"
        })
    ]
});

var progressList = new sap.m.List({
    items: [
        new sap.m.CustomListItem({
            type: "Active",
            content: [
                progressbarBar
            ],
            press: function(evt){
                toPageHistorie(evt);
            }

        })
    ]
});

var auswertungBar = new sap.m.Bar({
    width: "100%",
    contentMiddle: [
        new sap.m.Button({
            text: "Trainingsplan",
            widht: "95%",
            press: function(evt){
                toUebung(evt);
            }
        })
    ]
});

// ---------------- PAGE-AUSWERTUNGDETAIL ------------------- ---------------- PAGE-AUSWERTUNGDETAIL ------------------- ---------------- PAGE-AUSWERTUNGDETAIL -------------------
var testDescList2 = new sap.m.List();

var qInfoList = new sap.m.List({
    items: [
        new sap.m.CustomListItem({
            type: "Active",
            content: [
                new sap.m.FlexBox({
                    height: "80px",
                    alignItems: "Center",
                    items: [
                        new sap.ui.core.Icon({
                        color: "green",
                        src: "sap-icon://accept"
                        }),
                        new sap.m.Text({
                            text: "Beine druchgestreckt"
                        }),
                        new sap.ui.core.Icon({
                            src: sap.ui.core.IconPool.getIconURI("message-information"),
                            press: function(evt){
                                createQinfo(evt, false);
                            }
                        })
                    ]
                })

            ]
        }),
        new sap.m.CustomListItem({
            type: "Active",
            content: [
                new sap.ui.core.Icon({
                    color: "red",
                    src: sap.ui.core.IconPool.getIconURI("decline")
                }),
                new sap.m.Text({
                    text: "Rücken gerade"
                }),
                new sap.ui.core.Icon({
                    src: sap.ui.core.IconPool.getIconURI("message-information"),
                    press: function(evt){
                        createQinfo(evt, false);
                    }
                })
            ]
        }),
        new sap.m.CustomListItem({
            type: "Active",
            content: [
                new sap.ui.core.Icon({
                    color: "green",
                    src: sap.ui.core.IconPool.getIconURI("accept")
                }),
                new sap.m.Text({
                    text: "Beine druchgestreckt"
                }),
                new sap.ui.core.Icon({
                    src: sap.ui.core.IconPool.getIconURI("message-information"),
                    press: function(evt){
                        createQinfo(evt, false);
                    }
                })
            ]
        })
    ]
});

var auswDetailToolbar = new sap.m.Toolbar({
    content: [
        new sap.m.Button({
            text: "Zurück",
            icon: sap.ui.core.IconPool.getIconURI("arrow-left"),
            iconFirst: true
        }),
        new sap.m.ToolbarSpacer(),
        new sap.m.Button({
            text: "Weiter",
            icon: sap.ui.core.IconPool.getIconURI("arrow-right"),
            iconFirst: false
        })
    ]
});
// -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



var planBar = new sap.m.Bar({
    contentRight: new sap.m.Button({
        text: "Beginnen",
        press: function(evt){
            toUebung(evt);
        }
    })
});
// -----------------------------------------------------------------------------------------------------------------------------------------------



// ------- PAGE-ÜBUNGDETAILS -------- ------- PAGE-ÜBUNGDETAILS -------- ------- PAGE-ÜBUNGDETAILS -------- ------- PAGE-ÜBUNGDETAILS --------
var uCarousel = new sap.m.Carousel({
    height: "20%",
});

uCarousel.bindAggregation("pages", {
    path: "/",
    sorter: new sap.ui.model.Sorter("Lfdnr"),
    template: new sap.m.Image({
        height: "200px",
        src: {
            parts: [
                { path: "Url"},
                { path: "Lfdnr"}
            ],
            formatter: function(url, lfdnr){
                return "img/" + url.toLowerCase();
            }
        }
    })
});

var uDetailDesc = new sap.ui.core.HTML();

var uInfoPanel = new sap.m.Panel({
    headerText: "Butterfly",
    content: [
        uCarousel, uDetailDesc
    ]
}).addStyleClass('flexPadding');


// -------------------------------------------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------------------------------------------



// ---------------- PAGE-UEBUNG ------------------ ---------------- PAGE-UEBUNG ------------------ ---------------- PAGE-UEBUNG ---------------


// var uebungTitle = new sap.ui.core.HTML({
//     content: "<h3><font color='green'>" + (currSatz+1) + "/" + levelData.Satz + "</font> {/Text}</h3>"
// });

var uebungTitle = new sap.ui.core.HTML();

var uTitelFlexBox = new sap.m.FlexBox({
    alignItems: "Start",
    justifyContent: "SpaceBetween",
    width: "90%",
    items: [
        uebungTitle,
        new sap.ui.core.Icon({
            color: "green",
            src: "sap-icon://message-information"
        })
    ]
}).addStyleClass('flexMargin');

var uebungCarousel = new sap.m.Carousel({
    height: "10%"
});

var uProgBar1 = new sap.m.ProgressIndicator({
    height: "10px",
    widht: "100%",
    percentValue: 50
}).addStyleClass('noMargin');

var uProgBar2 = new sap.m.ProgressIndicator({
    height: "10px",
    widht: "100%",
    percentValue: 69
}).addStyleClass('noMargin');



var levelDetails = new sap.m.Text().addStyleClass('levelDetails');

var levelDetailBox = new sap.m.FlexBox({
    justifyContent: "Center",
    items: levelDetails
});

uebungCarousel.bindAggregation("pages", {
    path: "/",
    sorter: new sap.ui.model.Sorter("Lfdnr"),
    template: new sap.m.Image({
        height: "200px",
        src: {
            parts: [
                { path: "Url"},
                { path: "Id"},
                { path: "Lfdnr"}
            ],
            formatter: function(url, id, lfdnr){
                if (url != null)
                return "img/" + url.toLowerCase();
            }
        }
    })
});



function bindUebItems(isUebDecrement){
    currentUebung = selectedPlan[currentUebungKatIndex].uebungen[currentUebungIndex];
    pageUebung.getModel().setData(currentUebung);
    levelData = _.findWhere(planLevelData, { 'Screeningid': currentUebung.Id});
    var textLevel = levelData.Wdh + " * " + currentUebung.Text;
    // alert("levelDetails: " + textLevel);
    levelDetails.setText(textLevel);

    if (isUebDecrement){
        currSatz = levelData.Satz - 1;
    }
    uebungTitle.setContent("<h3><font color='green'>" + (currSatz + 1) + "/" + levelData.Satz + "</font> " + app.getCurrentPage().getModel().getObject('/Text') + "</h3>");
    //alert(levelDetails.getText());
}

var uBar = new sap.m.Toolbar({
    content: [
        new sap.m.Button({
            text: "Zurück",
            icon: sap.ui.core.IconPool.getIconURI("arrow-left"),
            iconFirst: true,
            press: function(evt){
                if (currSatz > 0){
                    currSatz--;
                    uebungTitle.setContent("<h3><font color='green'>" + (currSatz + 1) + "/" + levelData.Satz + "</font> " + app.getCurrentPage().getModel().getObject('/Text') + "</h3>");
                    uProgBar1.setPercentValue(((currentUebungKatIndex + 1) / selectedPlan.length) * 100);
                } else {
                    if (currentUebungIndex > 0){
                        currentUebungIndex--;
                        bindUebItems(true);
                        var cU = selectedPlan[currentUebungKatIndex].uebungen[currentUebungIndex];
                        getUebungMediaData(cU.Id, true);
                        uProgBar1.setPercentValue(((currentUebungKatIndex + 1) / selectedPlan.length) * 100);
                        uProgBar2.setPercentValue(((currentUebungIndex + 1) / selectedPlan[currentUebungKatIndex].uebungen.length) * 100);
                    } else {
                        if (currentUebungKatIndex > 0){
                            currentUebungKatIndex--;
                            currentUebungIndex = selectedPlan[currentUebungKatIndex].uebungen.length - 1;
                            bindUebItems(true);
                            // alert("currentUebungKatIndex: " + currentUebungKatIndex + ", currentUebungIndex: " + currentUebungIndex + ", uebungen: " + selectedPlan[currentUebungKatIndex].uebungen.length);
                            getLevelData(selectedPlan[currentUebungKatIndex].uebungen[currentUebungIndex].Id, level);
                            pageUebung.setTitle((currentUebungKatIndex + 1) + "/" + selectedPlan.length + " " + selectedPlan[currentUebungKatIndex].Text);
                            uProgBar1.setPercentValue(((currentUebungKatIndex + 1) / selectedPlan.length) * 100);
                            uProgBar2.setPercentValue(((currentUebungIndex + 1) / selectedPlan[currentUebungKatIndex].uebungen.length) * 100);
                        } else {
                            currentUebungKatIndex = 0;
                            app.backToPage(pageMenu);
                            getMenuData();
                            menuVarInit();
                            pagePlan.destroyContent();
                            //uebungCarousel.removeAllPages();
                            uebungCarousel.destroyPages();
                        }
                    }
                }
            }
        }),
        new sap.m.ToolbarSpacer(),
        new sap.m.Button({
            text: "Weiter",
            icon: sap.ui.core.IconPool.getIconURI("arrow-right"),
            iconFirst: false,
            press: function(evt){
                if (currSatz + 1 < levelData.Satz){
                    currSatz++;
                    uebungTitle.setContent("<h3><font color='green'>" + (currSatz + 1) + "/" + levelData.Satz + "</font> " + app.getCurrentPage().getModel().getObject('/Text') + "</h3>");

                } else {
                    currSatz = 0;
                    if ((currentUebungIndex + 1) < selectedPlan[currentUebungKatIndex].uebungen.length){
                        // alert("0");
                        currentUebungIndex++;
                        bindUebItems(false);
                        var cU = selectedPlan[currentUebungKatIndex].uebungen[currentUebungIndex];
                        getUebungMediaData(cU.Id, true);
                        uProgBar1.setPercentValue(((currentUebungKatIndex + 1) / selectedPlan.length) * 100);
                        uProgBar2.setPercentValue(((currentUebungIndex + 1) / selectedPlan[currentUebungKatIndex].uebungen.length) * 100);
                        // alert(((currentUebungIndex + 1) / selectedPlan[currentUebungKatIndex].uebungen.length) * 100);
                    } else {
                        currentUebungIndex = 0;
                        if ((selectedPlan.length > 1) && (selectedPlan.length > (currentUebungKatIndex + 1))){
                             // alert("setPercentValue()");
                            currentUebungKatIndex++;
                            getLevelData(selectedPlan[currentUebungKatIndex].uebungen[currentUebungIndex].Id, level);
                            pageUebung.setTitle((currentUebungKatIndex + 1) + "/" + selectedPlan.length + " " + selectedPlan[currentUebungKatIndex].Text);
                            uProgBar1.setPercentValue(((currentUebungKatIndex + 1) / selectedPlan.length) * 100);
                            uProgBar2.setPercentValue(((currentUebungIndex + 1) / selectedPlan[currentUebungKatIndex].uebungen.length) * 100);
                        } else {
                            currentUebungKatIndex = 0;
                            app.backToPage(pageMenu);
                            getMenuData();
                            menuVarInit();
                            pagePlan.destroyContent();
                            // uebungCarousel.removeAllPages();
                            uebungCarousel.destroyPages();
                        }
                    }
                }
            }
        })
    ]
});
// --------------------------------------------------------------------------------------------------------------------------------------------

function createTestInfo(){
    app.to(pageTestInfo);
    pageTestInfo.setTitle(pageTest.getTitle());
    getTestLangTextData(currentTests[currentTestIndex].Id, "info");
    testInfoPanel.setHeaderText(currentTests[currentTestIndex].Text);
}

function createAuswertung(evt){
    app.to(pageAuswertung);
}

function createAuswDetail(evt){
    app.to(pageAuswDetail);
     var testDescItem2 = new sap.m.StandardListItem({
        title: "3/5 Butterfly",
        icon: "img/descImage.png",
        type: "Navigation",
        press: function(evt){
            createTestInfo();
        }
    });
    testDescList2.removeAllItems();
    testDescList2.addItem(testDescItem2);
}

function createQinfo(evt, withBtns, toggle){
    switchItem = toggle;
    app.to(pageQuestionInfo);
    if (withBtns){
         qInfoToolbar.setVisible(true);
    } else {
        qInfoToolbar.setVisible(false);
    }
}

function setTestDescItem(text){
    (testDescList.getItems()[0]).setDescription(text);
}

